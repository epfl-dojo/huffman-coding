class SortableByWeight
  def initialize
    @code = ''
  end

  def weight
    @weight
  end

  def code
    @code
  end

  def code= code
    @code = code
  end
end

class Leaf < SortableByWeight
  def letter
    @letter
  end

  def initialize(letter, weight)
    super()
    @letter = letter
    @weight = weight
  end
end

class Node < SortableByWeight
  def right
    @right
  end

  def left
    @left
  end

  def initialize(right, left, weight)
    super()
    @right = right
    @left = left
    @weight = weight
  end
end

def build_tree word
  # Unique hash of letters
  letters={}
  word.split("").each do |char|
    letters[char] ||=0
    letters[char] +=1
  end

  # Array of leaves
  leaves = []
  letters.each do |char, weight|
    leaf = Leaf.new(char, weight)
    leaves << leaf
  end

  # Sort array (smallest to biggest)
  leaves = leaves.sort_by { |leaf| leaf.weight }

  # Build the tree
  while leaves.count > 1
    right = leaves[0]
    left = leaves[1]
    weight = leaves[0].weight + leaves[1].weight
    node = Node.new right, left, weight
    (1..2).each { leaves.shift }
    leaves << node
    leaves = leaves.sort_by { |leaf| leaf.weight }
  end

  leaves[0]
end

def gen_table_recursive node, table = {}
  node.right.code = "#{node.code}1"
  node.left.code  = "#{node.code}0"

  if node.right.is_a? Leaf
    table[node.right.letter] = node.right.code
    p table
  else
    table.merge gen_table_recursive(node.right, table)
  end

  if node.left.is_a? Leaf
    table[node.left.letter] = node.left.code
    p table
  else
    table.merge gen_table_recursive(node.left, table)
  end

  table
end

def gen_table_loop tree
  nodes_array = [tree]
  table       = {}

  while nodes_array.count > 0
    current_node = nodes_array[0]

    if current_node.is_a? Leaf
      table[current_node.letter] = current_node.code
      p table
    else
      current_node.right.code = "#{current_node.code}1"
      nodes_array << current_node.right
      current_node.left.code = "#{current_node.code}0"
      nodes_array << current_node.left
    end

    nodes_array.shift
  end

  table
end
def encode_string_ascii string
  encoded_string = ""
  string.each_byte do |byte|
    encoded_string << byte.to_s(2)
  end
  encoded_string
end
def encode_string string, table
  encoded_string = ""
  string.split("").each do |char|
    encoded_string << table[char]
  end
  encoded_string
end

def decode_compressed_string encoded_string, table
  decoded_string = ""
  table = table.invert
  while encoded_string.length > 0
    for i in 0..(encoded_string.length - 1)
      table.each do |code, char|
        if encoded_string[0..i] == code
          p "#{code}: #{char}"
          encoded_string[0..i] = ''
          decoded_string << char
        end
      end
    end
  end
  decoded_string
end

word = "Bonjour les petits poneys"

tree = build_tree word
table = gen_table_recursive tree
table = gen_table_loop tree
encoded_string = encode_string word, table
p encoded_string
p encode_string_ascii word
p decode_compressed_string(encoded_string, table)
